angular.module('sortjs').config(function($locationProvider) {
    $locationProvider.hashPrefix('');
});

angular.module('sortjs').config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html'
        })
        .when('/entry', {
            templateUrl: 'views/formEntry.html',
            controller: 'entryController'
        })
        .when('/drawn', {
            templateUrl: 'views/drawn.html',
            controller: 'raffleController'
        });
});