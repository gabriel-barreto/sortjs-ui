angular.module('sortjs').controller('raffleController', function($scope, $http, config) {
    $scope.raffle = {};
    $scope.info = {};

    angular.element(document).ready(function() {
        $http.get(config.devUrl + 'quantidade')
            .then(function successCallback(response) {
                $scope.info = response.data;
            })
            .then(function errorCallback(err) {
                if (err) console.log(err);
            });
    });

    $scope.drawn = function() {
        $http.get(config.devUrl + 'sorteado')
            .then(function successCallback(response) {
                $scope.raffle = response.data;
                console.log($scope.raffle);
                var objData = {
                    title: "você foi contemplado(a) pelo sorteio de dia das mães ProHouse!",
                    content: $scope.raffle,
                    message: "Parabéns de toda a equipe ProHouse!"
                }
                $scope.genModal(objData);
            })
            .then(function errorCallback(err) {
                if (err) console.log(err);
            });
    }

    $scope.genModal = function(objData) {
        var modal = $('#myModal');
        modal.on('show.bs.modal', function(e) {
            modal.find('div.modal-header').addClass('raffle');
            modal.find('h5.modal-title').text(objData.title);
            modal.find('p.modal-body-id').html("<span class='tag-label'>Id:</span> " + objData.content.id);
            modal.find('p.modal-body-name').html("<span class='tag-label'>Nome:</span> " + objData.content.name);
            modal.find('p.modal-body-city').html("<span class='tag-label'>Cidade:</span> " + objData.content.city);
            modal.find('p.modal-message').text(objData.message);
        });
        modal.modal('show');
    }
});