angular.module('sortjs').controller('entryController', function($scope, $http, config) {
    $scope.record = "";
    $scope.info = {};
    $scope.dataFile = "";
    angular.element(document).ready(function() {
        $scope.getQuantidade();
    });

    $scope.getQuantidade = function() {
        $http.get(config.devUrl + 'quantidade')
            .then(function successCallback(response) {
                $scope.info = response.data;
            })
            .then(function errorCallback(err) {
                if (err) {
                    console.log('Err: ' + response);
                }
            });
    }

    $scope.submit = function() {
        var formData = new FormData();
        var arquivo = document.getElementById("arquivoInput").files[0];
        formData.append("file", arquivo);
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                window.location.reload();
            }
        }
        xhr.open("POST", "http://localhost:4321/upload");
        xhr.send(formData);
    }

    /*$scope.insertRecord = function(record) {
        var req = {
            url: config.devUrl + "upload",
            header: { 'content-type': 'application/json', 'charset': 'utf-8' },
            method: "POST",
            data: record
        };
        $http(req)
            .then(function successCallback(response) {
                objData = {
                    status: 'success',
                    title: 'Dados inseridos com sucesso!',
                    content: 'Os dados informados foram cadastrados com sucesso!',
                    icon: 'fa fa-check'
                }
                $scope.genModal(objData);
            })
            .then(function errorCallback(err) {
                if (err) {
                    objData = {
                        status: 'error',
                        title: 'Dados não foram inseridos com sucesso!',
                        content: 'Os dados informados não puderam ser cadastrados com sucesso!\n' + err.data,
                        icon: 'fa fa-check'
                    }
                    $scope.genModal(objData);
                }
            });
    }
    $scope.genModal = function(objData) {
        var modal = $('#myModal');
        modal.on('show.bs.modal', function(e) {
            if (objData.status === 'success') {
                modal.find('div.modal-header').removeClass('error');
                modal.find('div.modal-header').addClass('success');
            } else {
                modal.find('div.modal-header').removeClass('success');
                modal.find('div.modal-header').addClass('error');
            }
            modal.find('h5.modal-title').text(objData.title);
            modal.find('p.modal-body-content').text(objData.content);
            modal.find('i#header-icon').removeClass();
            modal.find('i#header-icon').addClass(objData.icon);
        });
        modal.modal('show');
}*/
});