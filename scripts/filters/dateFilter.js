angular.module('sortjs').filter('dateFilter', function() {
    return function(input) {
        if (input.length > 0) {
            var day = input.substring(7, 10);
            var month = input.substring(4, 7);
            var year = input.substring(11, 15);
            var hour = input.substring(16, 18);
            var min = input.substring(19, 21);
            return (day + ' ' + month + ' ' + year + ' às ' + hour + 'h' + min);
        }
    };
});