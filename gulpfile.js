var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var runSequence = require('run-sequence');

'use strict'

gulp.task('sass', function() {
    return gulp.src('./stylesheets/scss/sytle.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./stylesheets/css'));
});

gulp.task('sass:watch', function() {
    return gulp.watch('./stylesheets/scss/*.scss', ['sass']);
});

gulp.task('default', function(callback) {
    return runSequence('sass', callback);
});